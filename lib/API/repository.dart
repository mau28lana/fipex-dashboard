import 'dart:convert';

import 'package:fipex_dashboard/API/model.dart';
import 'package:http/http.dart' as http;

class Repository {
  final url = "https://apis.ruang-ekspresi.id/fipex";

  //get methode

  Future getGuestdata() async {
    try {
      final response = await http.get(Uri.parse("$url/guests/books"));
      if (response.statusCode == 200) {
        Iterable it = jsonDecode(response.body);
        List<Guest> guest = it.map((e) => Guest.fromJson(e)).toList();

        return guest;
      }
    } catch (e) {}
  }

  Future getProducts() async {
    try {
      final response = await http.get(Uri.parse("$url/products"));
      if (response.statusCode == 200) {
        Iterable it = jsonDecode(response.body);
        List<Products> products = it.map((e) => Products.fromJson(e)).toList();

        return products;
      }
    } catch (e) {
      // print(e.toString());
    }
  }

  Future getCategories() async {
    try {
      final response = await http.get(Uri.parse("$url/categories"));
      if (response.statusCode == 200) {
        var body = jsonDecode(response.body);
        Iterable it = body;

        List<Category> categories =
            it.map((e) => Category.fromJson(e)).toList();

        return categories;
      }
    } catch (e) {}
  }

  Future getProductCategories({categoryId}) async {
    try {
      final response = await http
          .get(Uri.parse("$url/products/leaderboard/categories/$categoryId"));
      if (response.statusCode == 200) {
        Iterable it = jsonDecode(response.body);
        List<ProductsCategory> productsCategory =
            it.map((e) => ProductsCategory.fromJson(e)).toList();

        return productsCategory;
      }
    } catch (e) {
      // print(e.toString());
    }
  }
}
