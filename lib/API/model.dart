class Guest {
  final String id;
  final String comment;
  final String? createdAt;
  final String? updatedAt;
  final CreatedBy? createdBy;

  const Guest({
    required this.id,
    required this.comment,
    this.updatedAt,
    this.createdAt,
    this.createdBy,
  });

  factory Guest.fromJson(Map<String, dynamic> json) {
    return Guest(
      id: json['id'],
      comment: json['comment'],
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
      createdBy: CreatedBy.fromJson(json['created_by']),
    );
  }
}

class CreatedBy {
  String? id, name, email, bio, img;

  CreatedBy({
    this.id,
    this.name,
    this.email,
    this.bio,
    this.img,
  });

  factory CreatedBy.fromJson(dynamic json) {
    return CreatedBy(
        id: json['id'],
        name: json['name'],
        email: json['email'],
        bio: json['bio'],
        img: json['image_url']);
  }
}

class Products {
  String? id, name, description;
  String? categoryId, exhibitionId, authorId, createdAt, updatedAt;
  int? totalPoint;
  Thumbnails? thumbnails;

  Products({
    this.id,
    this.name,
    this.description,
    this.totalPoint,
    this.categoryId,
    this.exhibitionId,
    this.authorId,
    this.createdAt,
    this.updatedAt,
    this.thumbnails,
  });

  factory Products.fromJson(Map<String, dynamic> json) {
    return Products(
      id: json['id'],
      name: json['name'],
      description: json['description'],
      totalPoint: json['total_points'],
      categoryId: json['category_id'],
      exhibitionId: json['exhibition_id'],
      authorId: json['author_id'],
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
      thumbnails: json[{'thumbnails'}],
    );
  }
}

class ProductsCategory {
  String? id, name, description;
  int? totalPoint;
  Thumbnails? thumbnails;
  Category? category;

  ProductsCategory(
      {this.id,
      this.name,
      this.description,
      this.totalPoint,
      this.thumbnails,
      this.category});

  factory ProductsCategory.fromJson(Map<String, dynamic> json) {
    return ProductsCategory(
      id: json['id'],
      name: json['name'],
      description: json['description'],
      totalPoint: json['total_points'],
      thumbnails: json[{'thumbnails'}],
      category: Category.fromJson(json['category']),
    );
  }
}

class Thumbnails {
  List<String>? id, img;

  Thumbnails({
    this.id,
    this.img,
  });

  factory Thumbnails.fromJson(dynamic json) {
    return Thumbnails(
      id: json[0]['id'],
      img: json[0]['image_url'],
    );
  }
}

class Category {
  String? id, name;

  Category({
    this.id,
    this.name,
  });

  factory Category.fromJson(dynamic json) {
    return Category(
      id: json['id'],
      name: json['category_name'],
    );
  }
}

class Author {
  String? id, name, email, bio, avatar;

  Author({this.id, this.name, email, bio, avatar});

  factory Author.fromJson(dynamic json) {
    return Author(
        id: json['id'],
        name: json['name'],
        email: json['email'],
        bio: json['bio'],
        avatar: json['avatar']);
  }
}

class Token {
  String token;

  Token({required this.token});
  factory Token.fromJson(dynamic object) {
    return Token(token: object['token']);
  }
}
