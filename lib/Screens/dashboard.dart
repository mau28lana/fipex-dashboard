import 'dart:async';

import 'package:fipex_dashboard/API/model.dart';
import 'package:fipex_dashboard/API/repository.dart';
import 'package:fipex_dashboard/Widget/leaderboards_cotent.dart';
import 'package:fipex_dashboard/theme/color.dart';
import 'package:fipex_dashboard/theme/text_style.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  Repository repository = Repository();
  List<Guest> listGuest = [];
  // List<Products> listProducts = [];
  List<ProductsCategory> dp = [];
  List<ProductsCategory> dw = [];
  List<ProductsCategory> game = [];
  List<ProductsCategory> mm = [];
  List<ProductsCategory> ppl = [];

  List<Category> categories = [];

  int selectedIndex = 0;
  int filteringIndex = 0;

  getData() async {
    // listProducts = await repository.getProducts();
    listGuest = await repository.getGuestdata();
    categories = await repository.getCategories();
    dp = await repository.getProductCategories(categoryId: categories[0].id);
    dw = await repository.getProductCategories(categoryId: categories[1].id);
    game = await repository.getProductCategories(categoryId: categories[2].id);
    mm = await repository.getProductCategories(categoryId: categories[3].id);
    ppl = await repository.getProductCategories(categoryId: categories[4].id);

    // dp = listProducts
    //     .where((element) => element.categoryId == categories[1].id)
    //     .toList();
    // dw = listProducts
    //     .where((element) => element.categoryId == categories[2].id)
    //     .toList();
    // game = listProducts
    //     .where((element) => element.categoryId == categories[3].id)
    //     .toList();
    // mm = listProducts
    //     .where((element) => element.categoryId == categories[4].id)
    //     .toList();
    // ppl = listProducts
    //     .where((element) => element.categoryId == categories[5].id)
    //     .toList();
  }

  @override
  void initState() {
    super.initState();
    getData();
    Timer.periodic(Duration(minutes: 1), (timer) {
      setState(() {
        getData();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Row(children: [
        Container(
            margin: const EdgeInsets.fromLTRB(8, 8, 0, 8),
            child: getLeaderboards(size)),
        Column(
          children: [getGuestBook(size)],
        )
      ]),
    );
  }

  getLeaderboards(Size size) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(children: [
          getLeaderboardsCntent(size,
              title: "Peringkat Kategori Desain Produk",
              length: dp.length,
              // < 5 ? dp.length : 5,
              localIndex: 0),
          getLeaderboardsCntent(size,
              title: "Peringkat Kategori Desain web",
              length: dw.length,
              // < 5 ? dw.length : 5,
              localIndex: 1),
          getLeaderboardsCntent(size,
              title: "Peringkat Kategori Game",
              length: game.length,
              // < 5 ? game.length : 5,
              localIndex: 2),
        ]),
        Row(children: [
          getLeaderboardsCntent(size,
              title: "Peringkat Kategori Multimedia",
              length: mm.length,
              //  < 5 ? mm.length : 5,
              localIndex: 3),
          getLeaderboardsCntent(size,
              title: "Peringkat Kategori Pengembangan Perangkat Lunak",
              length: ppl.length,
              //  < 5 ? ppl.length : 5,
              localIndex: 4),
          getQrCode(size),
        ])
      ],
    );
  }

  getLeaderboardsCntent(Size size, {title, length, localIndex}) {
    return Container(
      margin: EdgeInsets.all(4),
      height: size.height * 0.47,
      width: size.width * 0.212,
      decoration: BoxDecoration(
          color: Colors.grey.shade300, borderRadius: BorderRadius.circular(6)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 6, 0, 0),
            child: Text(
              title,
              style: TextStyles(context).getBoldStyle().copyWith(fontSize: 8),
            ),
          ),
          Container(
            padding: EdgeInsets.all(2),
            height: size.height * 0.43,
            width: size.width * 0.33,
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: length,
              itemBuilder: (context, index) {
                return SizedBox(
                  height: 52,
                  child: Card(
                      child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        // localIndex == 0 && dp[index].thumbnails?.img != null
                        //     ? getImageProduct(
                        //         "https://apis.ruang-ekspresi.id/fipex${dp[index].thumbnails?.img}")
                        //     : localIndex == 1 &&
                        //             dw[index].thumbnails?.img != null
                        //         ? getImageProduct(
                        //             "https://apis.ruang-ekspresi.id/fipex${dw[index].thumbnails?.img}")
                        //         : localIndex == 2 &&
                        //                 game[index].thumbnails?.img != null
                        //             ? getImageProduct(
                        //                 "https://apis.ruang-ekspresi.id/fipex${game[index].thumbnails?.img}")
                        //             : localIndex == 3 &&
                        //                     mm[index].thumbnails?.img != null
                        //                 ? getImageProduct(
                        //                     "https://apis.ruang-ekspresi.id/fipex${mm[index].thumbnails?.img}")
                        //                 : ppl[index].thumbnails?.img != null
                        //                     ? getImageProduct(
                        //                         "https://apis.ruang-ekspresi.id/fipex${ppl[index].thumbnails?.img}")
                        //                     : SizedBox(
                        //                         width: 50,
                        //                         height: 50,
                        //                         child: Image.asset(
                        //                             "../../assets/image/Logo-RE-.jpg")),
                        Expanded(
                          child: Text(
                            localIndex == 0
                                ? dp[index].name!
                                : localIndex == 1
                                    ? dw[index].name!
                                    : localIndex == 2
                                        ? game[index].name!
                                        : localIndex == 3
                                            ? mm[index].name!
                                            : ppl[index].name!,
                            style: TextStyles(context)
                                .getBoldStyle()
                                .copyWith(fontSize: 8),
                          ),
                        ),
                        Container(width: 28),
                        Expanded(
                            child: Text(
                          "Total Points:  ${localIndex == 0 ? dp[index].totalPoint.toString() : localIndex == 1 ? dw[index].totalPoint.toString() : localIndex == 2 ? game[index].totalPoint.toString() : localIndex == 3 ? mm[index].totalPoint.toString() : ppl[index].totalPoint.toString()}",
                          style: TextStyles(context)
                              .getDescriptionStyle()
                              .copyWith(fontSize: 8),
                        ))
                      ],
                    ),
                  )),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  getQrCode(Size size) {
    return Container(
      padding: EdgeInsets.all(4),
      height: size.height * 0.46,
      width: size.width * 0.216,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Image.asset("../assets/image/fipex-exhibition-4.png"),
          Text(
            "Akses link berikut: https://fipexapp.ruang-ekspresi.id/ lalu Scan Barcode Di Atas Untuk Mengisi Buku tamu FiPEX#4",
            maxLines: 2,
            textAlign: TextAlign.center,
            style: TextStyles(context).getRegularStyle().copyWith(fontSize: 8),
          ),
        ],
      ),
    );
  }

  getGuestBook(Size size) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(top: 10, left: 12),
          child: Text(
            "Visitors",
            style: TextStyles(context).getRegularStyle(),
          ),
        ),
        Center(
          child: Container(
            padding: EdgeInsets.all(8),
            height: size.height * 0.94,
            width: size.width * 0.33,
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: listGuest.length,
              itemBuilder: (context, index) {
                return SizedBox(
                  height: 150,
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(18.0),
                      child: Row(children: [
                        listGuest[index].createdBy?.img != null
                            ? CircleAvatar(
                                minRadius: 30,
                                maxRadius: 30,
                                backgroundColor: white,
                                backgroundImage: NetworkImage(
                                    "https://apis.ruang-ekspresi.id/fipex/${listGuest[index].createdBy?.img}"),
                              )
                            : const CircleAvatar(
                                minRadius: 30,
                                maxRadius: 30,
                                backgroundImage: NetworkImage(
                                    "https://www.woolha.com/media/2020/03/eevee.png"),
                              ),
                        const SizedBox(
                          width: 24,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: 160,
                              child: Text(
                                listGuest[index].createdBy!.name.toString(),
                                maxLines: 2,
                                style: TextStyles(context)
                                    .getBoldStyle()
                                    .copyWith(fontSize: 16),
                              ),
                            ),
                            SizedBox(height: 4),
                            Expanded(
                              child: SizedBox(
                                height: 120,
                                width: size.width * 0.19,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      listGuest[index].comment.toString(),
                                      maxLines: 2,
                                      style: TextStyles(context)
                                          .getRegularStyle()
                                          .copyWith(fontSize: 12),
                                    ),
                                    SizedBox(height: 6),
                                    Text(
                                      "Created at: " +
                                          listGuest[index].createdAt.toString(),
                                      maxLines: 2,
                                      style: TextStyles(context)
                                          .getRegularStyle()
                                          .copyWith(fontSize: 10),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        )
                      ]),
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ],
    );
  }

  getImageProduct(image) {
    return SizedBox(width: 50, height: 50, child: Image.network(image));
  }
}
