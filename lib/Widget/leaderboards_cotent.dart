import 'package:fipex_dashboard/theme/text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';

class LeaderboardsContent extends StatefulWidget {
  final name;
  final point;
  final length;
  int? futureIndex;
  LeaderboardsContent(
      {super.key, this.length, this.name, this.point, this.futureIndex = 0});

  @override
  State<LeaderboardsContent> createState() => _LeaderboardsContentState();
}

class _LeaderboardsContentState extends State<LeaderboardsContent> {
  int localIndex = 0;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.all(4),
      height: size.height * 0.47,
      width: size.width * 0.212,
      decoration: BoxDecoration(
          color: Colors.grey.shade300, borderRadius: BorderRadius.circular(6)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 6, 0, 0),
            child: Text(
              'Peringkat Desain Web',
              style: TextStyles(context).getBoldStyle().copyWith(fontSize: 8),
            ),
          ),
          Container(
            padding: EdgeInsets.all(2),
            height: size.height * 0.43,
            width: size.width * 0.33,
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: widget.length,
              itemBuilder: (context, index) {
                widget.futureIndex = index;
                return SizedBox(
                  height: 52,
                  child: Card(
                      child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        SizedBox(
                            width: 50,
                            height: 50,
                            child: Image.asset("../assets/image/Logo-RE-.jpg")),
                        Expanded(
                          child: Text(
                            widget.name,
                            style: TextStyles(context)
                                .getBoldStyle()
                                .copyWith(fontSize: 8),
                          ),
                        ),
                        Container(width: 28),
                        Expanded(
                            child: Text(
                          widget.point,
                          style: TextStyles(context)
                              .getDescriptionStyle()
                              .copyWith(fontSize: 8),
                        ))
                      ],
                    ),
                  )),
                );
              },
            ),
          ),
        ],
      ),
    );
    ;
  }
}
